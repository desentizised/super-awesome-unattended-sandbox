cd /D "%~dp0"
::Remove Microsoft Edge Shortcut (because what could be more important)
del "%USERPROFILE%\Desktop\Microsoft Edge.lnk"
::Set Home Folder Icon
echo [.ShellClassInfo] > %USERPROFILE%\desktop.ini
echo IconResource=C:\Windows\System32\SHELL32.dll,150 >> %USERPROFILE%\desktop.ini
attrib +S +H +R -A %USERPROFILE%\desktop.ini
attrib +S +R %USERPROFILE%
::Stop Explorer
taskkill /im explorer.exe /f
::Set wallpaper
powershell -executionpolicy bypass -command "& { . .\setWallpaper.ps1; Set-WallPaper -Image %1 }"
::Set Taskbar small and buttons to never combine
reg add "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v TaskbarSmallIcons /t REG_DWORD /d 1 /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v TaskbarGlomLevel /t REG_DWORD /d 2 /f
::Set File Extensions visible
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v HideFileExt /t REG_DWORD /d 0 /f
::Set Hidden Files visible
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v Hidden /t REG_DWORD /d 1 /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v ShowSuperHidden /t REG_DWORD /d 1 /f
::Set Explorer to open to "This PC"
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v LaunchTo /t REG_DWORD /d 1 /f
::Add Desktop Icons
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" /v {20D04FE0-3AEA-1069-A2D8-08002B30309D} /t REG_DWORD /d 0 /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu" /v {20D04FE0-3AEA-1069-A2D8-08002B30309D} /t REG_DWORD /d 0 /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" /v {59031a47-3f72-44a7-89c5-5595fe6b30ee} /t REG_DWORD /d 0 /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu" /v {59031a47-3f72-44a7-89c5-5595fe6b30ee} /t REG_DWORD /d 0 /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" /v {F02C1A0D-BE21-4350-88B0-7367FC96EF3C} /t REG_DWORD /d 0 /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu" /v {F02C1A0D-BE21-4350-88B0-7367FC96EF3C} /t REG_DWORD /d 0 /f
::Remove Taskbar Pins (setting custom ones is not really possible through the registry so we will do that through chocolatey later)
reg add "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Taskband" /v Favorites /t REG_BINARY /d ff /f
reg add "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Taskband\AuxilliaryPins" /v MailPin /t REG_DWORD /d 1 /f
::Set all Taskbar Icons visible
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer" /v EnableAutoTray /t REG_DWORD /d 0 /f
::Enable Auto Arrange for Icons
reg delete "HKCU\Software\Microsoft\Windows\Shell\BagMRU" /F
reg delete "HKCU\Software\Microsoft\Windows\Shell\Bags" /F
reg delete "HKCU\Software\Microsoft\Windows\ShellNoRoam\Bags" /F
reg delete "HKCU\Software\Microsoft\Windows\ShellNoRoam\BagMRU" /F
reg delete "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\Shell\BagMRU" /F
reg delete "HKCU\Software\Classes\Local Settings\Software\Microsoft\Windows\Shell\Bags" /F
reg delete "HKCU\Software\Classes\Wow6432Node\Local Settings\Software\Microsoft\Windows\Shell\Bags" /F
reg delete "HKCU\Software\Classes\Wow6432Node\Local Settings\Software\Microsoft\Windows\Shell\BagMRU" /F
::Set User Shell Folders
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" /v "My Pictures" /t REG_EXPAND_SZ /d "%2" /F
rmdir /q /s "%USERPROFILE%\Pictures"
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" /v Personal /t REG_EXPAND_SZ /d "%3" /F
rmdir /q /s "%USERPROFILE%\Documents"
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" /v Favorites /t REG_EXPAND_SZ /d "%3\Favorites" /F
rmdir /q /s "%USERPROFILE%\Favorites"
::Start Explorer up again
start explorer.exe
RUNDLL32.EXE user32.dll,UpdatePerUserSystemParameters
::Set Open Dialog Shortcuts
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\comdlg32\Placesbar" /v Place0 /t REG_SZ /d "" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\comdlg32\Placesbar" /v Place1 /t REG_SZ /d "%USERPROFILE%\Desktop" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\comdlg32\Placesbar" /v Place2 /t REG_SZ /d "%USERPROFILE%" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\comdlg32\Placesbar" /v Place3 /t REG_SZ /d "%USERPROFILE%\Downloads" /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\comdlg32\Placesbar" /v Place4 /t REG_SZ /d "%3" /f
::Run every .exe/.msi in Folder of %1 (abandoned)
::for /r %~p1 %%a in (*.exe) do start "" "%%~fa"
::for /r %~p1 %%a in (*.msi) do start "" "%%~fa"
::Prepare Firefox Add-ons
xcopy .\Installers\*.xpi "C:\Program Files\Mozilla Firefox\distribution\extensions\"
::Install Chocolatey
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))"
SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
choco feature enable -n allowGlobalConfirmation
::First things first (Firefox and pins)
choco install syspin
syspin.exe "C:\Windows\explorer.exe" c:5386
syspin.exe "C:\Windows\System32\cmd.exe" c:5386
choco install setdefaultbrowser firefoxesr
SetDefaultBrowser.exe HKLM Firefox-308046B0AF4A39CB
syspin.exe "C:\Program Files\Mozilla Firefox\firefox.exe" c:5386
syspin.exe "C:\Windows\System32\mstsc.exe" c:5386
::Installers that take a while and have no dependencies
start /min choco install tor-browser chromium opera-developer --params '"/NoTaskbarShortcut"'
start /min choco install adobereader k-litecodecpackfull javaruntime
::Install admin software
start /min choco install sysinternals && choco install nirlauncher --params='"/Sysinternals"' && powershell -executionpolicy bypass -command "& { . .\createShortcuts.ps1; }"
choco install processhacker
::Set Process Hacker as default Task Manager
reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\taskmgr.exe" /v Debugger /t REG_SZ /d "\"C:\Program Files\Process Hacker 2\ProcessHacker.exe\"" /f
del "%USERPROFILE%\Desktop\Process Hacker 2.lnk"
::Set Classic Shell values
reg add "HKLM\Software\IvoSoft\ClassicStartMenu" /v MenuStyle /t REG_SZ /d "Classic1" /f
reg add "HKCU\Software\IvoSoft\ClassicStartMenu\Settings" /v SkinC1 /t REG_SZ /d "Smoked Glass" /f
reg add "HKCU\Software\IvoSoft\ClassicStartMenu\Settings" /v SkinC2 /t REG_SZ /d "Smoked Glass" /f
::Install auxiliary software
start /min choco install classic-shell notepad2 7zip openssh microsoft-windows-terminal 
choco install openvpn
del "%PUBLIC%\Desktop\OpenVPN GUI.lnk"
choco install teamviewer.host
del "%PUBLIC%\Desktop\TeamViewer Host.lnk"
choco install vlc-nightly
del "%PUBLIC%\Desktop\VLC media player.lnk"
choco install clipgrab
del "%PUBLIC%\Desktop\ClipGrab.lnk"
exit 0
