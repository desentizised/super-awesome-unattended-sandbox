$WScriptShell = New-Object -ComObject WScript.Shell

$SourceFileLocation = �C:\tools\NirLauncher\NirLauncher.exe�
$ShortcutLocation = �$env:USERPROFILE\Desktop\NirLauncher.lnk�
$Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)
$Shortcut.TargetPath = $SourceFileLocation
$Shortcut.Save()

$SourceFileLocation = �$env:ProgramData\chocolatey\lib\sysinternals\tools\procexp64.exe�
$ShortcutLocation = �$env:USERPROFILE\Desktop\Process Explorer.lnk�
$Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)
$Shortcut.TargetPath = $SourceFileLocation
$Shortcut.Save()